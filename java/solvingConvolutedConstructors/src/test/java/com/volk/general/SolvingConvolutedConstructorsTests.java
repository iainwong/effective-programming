package com.volk.general;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SolvingConvolutedConstructorsTests {

    @DisplayName("Car - Instantiation Test")
    @Test
    void carInstantiationTest() {
        SolvingConvolutedConstructors.createCars();
    }

    @DisplayName("Motorcycle - Instantiation Test")
    @Test
    void motorcycleInstantiationTest() {
        SolvingConvolutedConstructors.createMotorcycles();
    }
}
