package com.volk.general;

import com.volk.example.Car;
import com.volk.example.Car.Brand;
import com.volk.example.Motorcycle;
import com.volk.example.Vehicle.Feature;

/**
 * The builder technique is one way to simplify convoluted Java Constructors.
 *
 * Its general benefits include,
 *  - easy programming/readibility (as opposed to telescoping constructors)
 *  - consistent state (defaults are enforced)
 *  - the option to return immuatable objects
 *
 * It can be used to solve the following problems:
 * 1) named-optional parameters/default parameters (in Scala or Python)
 *      a) especially throughout class hierarchies
 */
public class SolvingConvolutedConstructors {
    public static void createCars() {
        Car toyota = new Car.Builder(Brand.HONDA)
                            .addFeature(Feature.AIR_CONDITIONING)
                            .addFeature(Feature.HEAT)
                            .addFeature(Feature.ELECTRIC_WINDOWS)
                            .build();
        Car honda = new Car.Builder(Brand.HONDA)
                            .addFeature(Feature.HEAT)
                            .build();
        Car subaru = new Car.Builder(Brand.SUBARU)
                            .addFeature(Feature.AIR_CONDITIONING)
                            .addFeature(Feature.ELECTRIC_WINDOWS)
                            .build();
    }
    public static void createMotorcycles() {
        Motorcycle unicycle = new Motorcycle.Builder()
                                .addFeature(Feature.AIR_CONDITIONING)
                                .aUnicycle()
                                .build();
    }
}
