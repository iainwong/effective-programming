package com.volk.example;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

public abstract class Vehicle {
    public enum Feature {AIR_CONDITIONING, HEAT, ELECTRIC_WINDOWS};
    final Set<Feature> features;
    
    /**
     * Recursive-type parameter.
     */
    abstract static class Builder<T extends Builder<T>> {
        EnumSet<Feature> features = EnumSet.noneOf(Feature.class);

        public T addFeature(Feature feature) {
            features.add(Objects.requireNonNull(feature));
            return self();
        }

        abstract Vehicle build(); 

        /**
         * Java simulated self-type idiom. Subclasses override and return 
         * "this".
         */
        protected abstract T self();
    }

    Vehicle(Builder<?> builder) {
        features = builder.features.clone();
    }
}
