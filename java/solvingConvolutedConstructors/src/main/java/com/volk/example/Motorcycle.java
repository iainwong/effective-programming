package com.volk.example;

public class Motorcycle extends Vehicle {
    private final boolean isUnicycle;

    public static class Builder extends Vehicle.Builder<Builder> {
        private boolean isUnicycle;

        public Builder aUnicycle() {
            isUnicycle = true;
            return this;
        }
        
        @Override public Motorcycle build() {
            return new Motorcycle(this);
        }

        @Override protected Builder self() { return this; }
    }

    private Motorcycle(Builder builder) {
        super(builder);
        this.isUnicycle = builder.isUnicycle;
    }
}
