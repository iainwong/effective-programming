package com.volk.example;

import java.util.Objects;

public class Car extends Vehicle {
    
    public enum Brand {HONDA, TOYOTA, SUBARU};
    private final Brand brand;

    public static class Builder extends Vehicle.Builder<Builder> {
        private final Brand brand;

        public Builder(Brand brand) {
            this.brand = Objects.requireNonNull(brand);
        }

        @Override public Car build() {
            return new Car(this);
        }

        @Override protected Builder self() { return this; }
    }

    private Car(Builder builder) {
        super(builder);
        this.brand = builder.brand;
    }
}
