package com.volk.general;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Disabled;

public class FactoryMethodsInsteadOfConstructorsTests {

    @DisplayName("no tests")
    @Test
    @Disabled
    void noTests() {
    }
}
