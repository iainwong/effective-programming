package com.volk.general;

/**
 * In addition to Constructors, a programmer can provide static factory methods
 * as a means of instantiation. This method is not the same as a Gang of Four
 * factory method. This is simply an augmentation or replacement of typical
 * constructor calls.
 *
 * Static method construction has the following advantages,
 *  1. their method names provide context, description and reliable usage
 *  2. not required to create a new object - providing instance control
 *  3. can return subtypes of their return-type - reducing conceptual weight
 *  4. the return object can differ based on the input params
 *  5. the static creation method can be developed independent of the return
 *  objects implementation. The return object does not need to exist during
 *  the instantiation method's development.
 *
 * Disadvantages,
 *  - if only static creation methods are provided, the class can not be
 *  subclassed
 *  - static creation methods are hard to find amongst the other methods.
 *  Whereas constructors are obviously creational. This can be mitigated by
 *  static creation method names like,
 *      - from - a type conversion method that takes a single param
 *      - of - an aggregation method that takes multiple params
 *      - valueOf - verbose form of from and of
 *      - instance/getInstance - return an instance
 *      - create/newInstance - guarantees the call returns a new instance
 *      - getType - used if factory method is in a different class
 *      - newType - like newInstance, but if factory method is in different
 *      class
 *      - type - short alternate to getType and newType
 */
public class FactoryMethodsInseadOfConstructors {

}
