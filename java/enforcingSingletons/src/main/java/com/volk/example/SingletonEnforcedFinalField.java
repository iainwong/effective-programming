package com.volk.example;

/**
 * A privatized constructor with a public static final singleton field clearly
 * indicates that this class is a singleton. It is short and sweet. This is the
 * second best option.
 */
public class SingletonEnforcedFinalField {

    public static final SingletonEnforcedFinalField INSTANCE =
        new SingletonEnforcedFinalField();
    
    private SingletonEnforcedFinalField() {/* do nothing */}
    
    public void doSomething() {/* do nothing */}
}
