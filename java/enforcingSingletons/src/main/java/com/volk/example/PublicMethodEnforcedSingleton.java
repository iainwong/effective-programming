package com.volk.example;

import java.beans.Transient;
import java.io.Serializable;

public class PublicMethodEnforcedSingleton implements Serializable {
    private static final PublicMethodEnforcedSingleton INSTANCE =
        new PublicMethodEnforcedSingleton();

    private transient String temporaryName;

    private PublicMethodEnforcedSingleton() {/* do nothing */}

    /**
     * Can have logic.
     *  - return separate instances per thread
     *  - can write generic singleton factory method
     *  - can be used a supplier method reference
     */
    public static PublicMethodEnforcedSingleton getInstance() {
        return INSTANCE;
    }

    public void doSomething() {/* do nothing */}
    
    /* to replace the read object from a stream - localized stub */
    private Object readResolve() {
        return INSTANCE;
    }
}
