package com.volk.example;

public enum EnumEnforcedSingleton {
    INSTANCE;

    public void doSomething() {/* do nothing */}
}
