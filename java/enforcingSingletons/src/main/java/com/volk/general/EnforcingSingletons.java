package com.volk.general;

import com.volk.example.SingletonEnforcedFinalField;
import com.volk.example.PublicMethodEnforcedSingleton;
import com.volk.example.EnumEnforcedSingleton;

/**
 * Singletons are classes which are instantiated one-time and provision
 * a function or a system component which is unique. Singletons can be
 * implemented by either using a private constructor or enum.
 * 
 * Enums are the preferred way due to shortness, free serialization machinery,
 * and neutralizes serialization or reflection attacks.
 *
 * A private constructor with a static creation method allows for,
 *  - subsequent refactoring
 *  - a generic singleton factory method
 *  - a method reference as a supplier
 *
 * A private constructor with a static final field allows for,
 *  - simplicity
 *  - obvious indication of a singleton
 */
public class EnforcingSingletons {

    public void enforceSingletons() {

        /* public static final member */
        SingletonEnforcedFinalField finalInstance = 
            SingletonEnforcedFinalField.INSTANCE;

        finalInstance.doSomething();

        /* public static method */
        PublicMethodEnforcedSingleton methodInstance =
            PublicMethodEnforcedSingleton.getInstance();

        methodInstance.doSomething();

        /* enum enforced singleton */
        EnumEnforcedSingleton enumInstance = EnumEnforcedSingleton.INSTANCE;
        enumInstance.doSomething();
    }
}
