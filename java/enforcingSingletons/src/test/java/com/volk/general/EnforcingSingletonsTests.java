package com.volk.general;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Disabled;

public class EnforcingSingletonsTests {

    @DisplayName("Conjure-up some singletons")
    @Test
    void singletonTest() {
        EnforcingSingletons enforcer = new EnforcingSingletons(); 
        enforcer.enforceSingletons();
    }
}
