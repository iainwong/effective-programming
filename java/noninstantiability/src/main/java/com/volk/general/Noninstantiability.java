package com.volk.general;

import com.volk.example.PrivatizedConstructor;

public class Noninstantiability {

    public void doSomething() {
        PrivatizedConstructor.doSomething();
    }

}
