package com.volk.example;

import java.lang.AssertionError;

public class PrivatizedConstructor {

    /**
     * Privatized constructor preventing instantiation from outside or
     * inside the class. The AssertionError prevents this class from performing
     * any instantiation.  */
    private PrivatizedConstructor() { throw new AssertionError(); }

    public static void doSomething() {/* do nothing */}
}
