package com.volk.general;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import com.volk.general.Noninstantiability;

public class NoninstantiabilityTests {

    @DisplayName("noninstantiability tests")
    @Test
    void noninstantiableTests() {
        Noninstantiability i = new Noninstantiability();
        i.doSomething();
    }
}
