package com.volk.general;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import com.volk.general.DependencyInjectionViaSupplier;

public class DependencyInjectionViaSupplierTests {

    @DisplayName("Supplier Dependency Injection Tests")
    @Test
    void testSupplierInject() {
        DependencyInjectionViaSupplier supplier =
            new DependencyInjectionViaSupplier();

        supplier.doSomething();
    }
}
