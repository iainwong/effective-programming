package com.volk.example;

import java.util.function.Supplier;

public class SupplyInjectedUtility {

    private Supplier<? extends String> injectedResource;

    public SupplyInjectedUtility() {
        this.injectedResource = null;
    }

    public SupplyInjectedUtility(Supplier<? extends String> injectedResource) {
        this();
        this.injectedResource = injectedResource;
    }
}
