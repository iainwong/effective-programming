package com.volk.general;

import com.volk.example.SupplyInjectedUtility;

/**
 * Useful variant of dependency injection is by passing a resource factory
 * to the constructor. These factories can be implemented using the
 * Gang of Four Factory Method pattern.
 */
public class DependencyInjectionViaSupplier {

    public void doSomething() {
        SupplyInjectedUtility utility = 
            new SupplyInjectedUtility(() -> "something");
    }
}
